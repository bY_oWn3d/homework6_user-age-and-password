//Теоретичні питання

//1.Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

//Щоб використовувати спеціальний символ як звичайний, додайте до нього зворотну косу межу: "\.."
//Це називається «екранування символу»
//alert( "Глава 5.1".match(/\d\.\d/) ); // 5.1 (совпадение!)
//alert( "Глава 511".match(/\d\.\d/) ); // null ("\." - ищет обычную точку)

//2.Які засоби оголошення функцій ви знаєте?
//function showMessage (){}
//const fn = function(){}

//3.Що таке hoisting, як він працює для змінних та функцій?
//Hoisting являє собою процес доступу до змінних до їх визначення.

//console.log(foo);
//Его выполнение вызовет ошибку ReferenceError: foo is not defined
//Добавим определение переменной:
//console.log(foo);   // undefined
//var foo = "Tom";

//В этом случае консоль выведет значение "undefined". При первом проходе компилятор узнает про существование переменной foo. Она получает значение undefined. При втором проходе вызывается метод console.log(foo).

//Завдання

function createNewUser() {
    const newUser = {
        firstName: "",
        lastName: "",
        newFirstName(name) {
            Object.defineProperty(newUser, "firstName", {
                value: name,
            });
        },
        newLastName(name) {
            Object.defineProperty(newUser, "lastName", {
                value: name,
            });
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            if (this.birthday) {
                const currentYear = new Date()

                //Перевірка на введення данних

                if (this.birthday.getMonth() >= currentYear.getMonth() && this.birthday.getDay() > currentYear.getDay()) { 
                    return `${this.firstName + ' ' + this.lastName + '\'s'} age is ${currentYear.getFullYear() - this.birthday.getFullYear() + 1}`
                }
                return `${this.firstName + ' ' + this.lastName + '\'s'} age is ${currentYear.getFullYear() - this.birthday.getFullYear()}`
            }
        },
        getPassword() {
            if (this.firstName && this.lastName && this.birthday) {
                return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear()
            }
        }
    };
    Object.defineProperty(newUser, "firstName", {
        writable: false,
        configurable: true,
        value: getUserName("Введіть ваше ім'я"),
    })
    Object.defineProperty(newUser, "lastName", {
        writable: false,
        configurable: true,
        value: getUserName("Введіть ваше прізвище"),
    })
    newUser.birthday = getDateOfBirth()
    return newUser;
}

function getUserName(question) {
    let firstName = prompt(`${question}`);
    if (firstName) {
        while (!firstName || !firstName.match(/^[0-9a-zA-Z]+$/)) {
            firstName = prompt(`${question}`, [firstName]).trim();
        }
        return firstName;
    }
}

function getDateOfBirth() {
    let inputUserDate = prompt("Введіть ваш вік в форматі (день, місяць, рік)")
    while (!/^[0-9]{1,2}\.[0-9]{1,2}\.[0-9]{4}$/.test(inputUserDate)) {
        alert("Помилка, введіть коректно")
        inputUserDate = prompt("Введіть ваш вік в форматі (день, місяць, рік)", [inputUserDate])
    }
    const dateParts = inputUserDate.split('.') 

    return new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
}

let newUser = createNewUser()
console.log(newUser)
console.log(newUser.getAge())
console.log(newUser.getPassword())
